package by.epam.threadexceptions.tests;

public class IllegalMonitorStateExceptionTest {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            public void run() {
                synchronized (Object.class) {
                    try {
                        wait();
                    } catch (IllegalMonitorStateException e) {
                        System.out.println("IllegalMonitorStateException");
                    } catch (InterruptedException exception) {
                        System.out.println("InterruptedException");
                    }
                }
            }
        }).start();

    }
}
