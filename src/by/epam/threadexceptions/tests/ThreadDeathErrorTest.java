package by.epam.threadexceptions.tests;

public class ThreadDeathErrorTest {
    public static void main(String[] args) {
        try {
            Thread.currentThread().stop();
        } catch (ThreadDeath threadDeath) {
            System.out.println("Thread Death Error");
        }
    }
}
