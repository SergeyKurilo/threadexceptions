package by.epam.threadexceptions.tests;

import by.epam.threadexceptions.thread.TestThread;

public class InterruptedExceptionTest {
    public static void main(String[] args) {
        TestThread testThread1 = new TestThread();
        testThread1.start();
        testThread1.interrupt();
    }
}
