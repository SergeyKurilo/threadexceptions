package by.epam.threadexceptions.tests;

import by.epam.threadexceptions.thread.TestThread;

public class IllegalThreadStateExceptionTest {
    public static void main(String[] args) {
        TestThread testThread = new TestThread();
        testThread.start();
        try {
            testThread.start();
        } catch (IllegalThreadStateException  e) {
            System.out.println("IllegalThreadStateException");
        }
    }
}
