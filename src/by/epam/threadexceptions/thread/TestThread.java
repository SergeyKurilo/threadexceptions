package by.epam.threadexceptions.thread;

public class TestThread extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 4; i++)  {
            System.out.println(i);
            try {
                sleep(3000);
            } catch (InterruptedException e) {
                System.out.println("InterruptedException");
            }
        }
    }
}
